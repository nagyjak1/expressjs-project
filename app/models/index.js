const sequelize = require('../utils/database');

const logger = require('../utils/logger');

const Service = require('./service');
const User = require('./user');
const Customer = require('./customer');

Customer.hasMany(Service);
Service.belongsTo(Customer);

User.belongsTo(Customer);
Customer.hasMany(User, {
    foreignKey: {
        name: 'customerId',
        allowNull: true
    }
});

async function sync() {
    await sequelize.query('SET FOREIGN_KEY_CHECKS = 0');
    await sequelize.sync({force: false});
    await sequelize.query('SET FOREIGN_KEY_CHECKS = 1');
    logger.info('Database synchronised');
}

module.exports = {
    sequelize,
    Customer,
    User,
    Service,
    sync,
};
