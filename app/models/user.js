const Sequelize = require('sequelize');
const sequelize = require('../utils/database');

/**
 * @swagger
 * components:
 *   schemas:
 *     User:
 *       type: object
 *       properties:
 *         id:
 *           type: integer
 *         username:
 *           type: string
 *         email:
 *           type: string
 *         role:
 *           type: string
 *           enum: [customer, admin]
 *         customerId:
 *           type: integer
 *
 *     CreateUser:
 *       type: object
 *       required:
 *         - username
 *         - email
 *         - password
 *         - customerId
 *       properties:
 *         username:
 *           type: string
 *         email:
 *           type: string
 *         password:
 *           type: string
 *         role:
 *           type: string
 *           enum: [customer, admin]
 *         customerId:
 *           type: integer
 *
 *     EditUser:
 *       type: object
 *       required:
 *         - username
 *         - email
 *         - password
 *         - role
 *         - customerId
 *       properties:
 *         username:
 *           type: string
 *         email:
 *           type: string
 *         password:
 *           type: string
 *         role:
 *           type: string
 *           enum: [customer, admin]
 *         customerId:
 *           type: integer
 */
const User = sequelize.define(
    'user',
    {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
        },
        username: {
            type: Sequelize.STRING,
            unique: true,
            allowNull: false,
        },
        password: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        email: {
            type: Sequelize.STRING,
            unique: true,
            isEmail: true,
            allowNull: false,
        },
        role: {
            type: Sequelize.ENUM('customer', 'admin'),
            allowNull: false,
            defaultValue: 'customer',
        }
    },
    {
        defaultScope: {
            attributes: {exclude: ['password']},
        },
        scopes: {
            withPassword: {}
        }
    }
);


module.exports = User