/**
 * @swagger
 * components:
 *   schemas:
 *     CreateCustomer:
 *       type: object
 *       required:
 *         - name
 *         - phoneNumber
 *       properties:
 *         name:
 *           type: string
 *           description: The name of the customer
 *         address:
 *           type: string
 *           description: The address of the customer
 *         phoneNumber:
 *           type: string
 *           description: The phone number of the customer
 *
 *     Customer:
 *       type: object
 *       properties:
 *         id:
 *           type: number
 *           description: The auto-generated id of the book
 *         name:
 *           type: string
 *           description: The name of the customer
 *         address:
 *           type: string
 *           description: The address of the customer
 *         phoneNumber:
 *           type: string
 *           description: The phone number of the customer
 *
 *     EditCustomer:
 *       type: object
 *       required:
 *         - name
 *         - address
 *         - phoneNumber
 *       properties:
 *         name:
 *           type: string
 *           description: The name of the customer
 *         address:
 *           type: string
 *           description: The address of the customer
 *         phoneNumber:
 *           type: string
 *           description: The phone number of the customer
 *
 *
 */

const Sequelize = require('sequelize');
const sequelize = require('../utils/database');

const Customer = sequelize.define('customer', {
    id: {
        type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true,
    }, name: {
        type: Sequelize.STRING, allowNull: false, unique: true,
    }, address: {
        type: Sequelize.STRING,
    }, phoneNumber: {
        type: Sequelize.STRING, unique: true, allowNull: false,
    },
});

module.exports = Customer;