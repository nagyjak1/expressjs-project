const Sequelize = require('sequelize');
const sequelize = require('../utils/database');


/**
 * @swagger
 * components:
 *   schemas:
 *     Service:
 *       type: object
 *       properties:
 *         id:
 *           type: integer
 *         name:
 *           type: string
 *         description:
 *           type: string
 *         status:
 *           type: string
 *           enum: [active, inactive]
 *         customerId:
 *           type: integer
 *
 *     CreateService:
 *       type: object
 *       required:
 *         - name
 *         - customerId
 *       properties:
 *         name:
 *           type: string
 *         description:
 *           type: string
 *         status:
 *           type: string
 *           enum: [active, inactive]
 *         customerId:
 *           type: integer
 *
 *     EditService:
 *       type: object
 *       required:
 *         - name
 *         - description
 *         - status
 *         - customerId
 *       properties:
 *         name:
 *           type: string
 *         description:
 *           type: string
 *         status:
 *           type: string
 *           enum: [active, inactive]
 *         customerId:
 *           type: integer
 */
const Service = sequelize.define(
    'service',
    {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        description: {
            type: Sequelize.TEXT,
        },
        status: {
            type: Sequelize.ENUM('active', 'inactive'),
            defaultValue: 'active',
        },
    }
);


module.exports = Service;