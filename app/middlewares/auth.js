const jwt = require('jsonwebtoken');

exports.authenticate = (req, res, next) => {
    let token = req.headers.authorization;
    if (!token) {
        const error = new Error('No token provided!');
        error.httpStatusCode = 401;
        throw error;
    }
    token = token.split(' ')[1];
    try {
        req.user = jwt.verify(token, process.env.SECRET).user;
        next();
    } catch (ex) {
        const error = new Error('Token is invalid or expired');
        error.httpStatusCode = 400;
        throw error;
    }
};

exports.isAdmin = (req, res, next) => {
    if (!req.user) {
        const error = new Error('User was not found. Server did not authenticate your request.');
        error.httpStatusCode = 401;
        throw error;
    }

    if (req.user.role === 'admin') next(); else {
        const error = new Error('Access Denied!');
        error.httpStatusCode = 403;
        throw error;
    }
}

exports.isRequestingOwnData = (req, res, next) => {
    if (!req.user) {
        const error = new Error('User was not found. Server did not authenticate your request.');
        error.httpStatusCode = 401;
        throw error;
    }
    if (req.user.role === 'admin' || req.user.id === req.params.id) next(); else {
        const error = new Error('Access Denied! This resource does not belong to you.');
        error.httpStatusCode = 403;
        throw error;
    }
}

exports.isRequestingAssignedCustomersData = (req, res, next) => {
    if (!req.user) {
        const error = new Error('User was not found. Server did not authenticate your request.');
        error.httpStatusCode = 401;
        throw error;
    }

    if (req.user.role === 'admin' || req.user.customerId.toString() === req.params.id) next(); else {
        const error = new Error('Access Denied! This resource does not belong to you.');
        error.httpStatusCode = 403;
        throw error;
    }
}