const logger = require('../utils/logger');
const morgan = require('morgan');

module.exports = morgan((tokens, req, res) => {
    const log = [
        tokens.method(req, res),
        tokens.url(req, res),
        tokens.status(req, res),
        tokens['response-time'](req, res) + 'ms',
        req.ip,
    ].join(' ');

    logger.info(log);
})