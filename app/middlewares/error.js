module.exports = (error, req, res, next) => {
    const status = error.httpStatusCode ? error.httpStatusCode : 500;
    const message = error.message;
    const data = error.data;
    res.status(status).json({message: message, data: data});
}