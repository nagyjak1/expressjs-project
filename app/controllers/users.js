const userService = require('../services/users');

exports.createUser = async (req, res, next) => {
    try {
        const email = req.body.email;
        const username = req.body.username;
        const password = req.body.password;
        const customerId = req.body.customerId;
        const role = req.body.role;
        let user = await userService.create(username, email, password, role, customerId);
        res.status(201).json(user);
    } catch (err) {
        next(err);
    }
}


exports.getAllUsers = async (req, res, next) => {
    try {
        const users = await userService.getAll();
        res.json(users);
    } catch (error) {
        next(error);
    }
}



exports.getUserById = async (req, res, next) => {
    try {
        const id = req.params.id;
        const user = await userService.getById(id)
        res.json(user);
    } catch (error) {
        next(error);
    }
}

exports.updateUser = async (req, res, next) => {
    try {
        const userId = req.params.id;
        const email = req.body.email;
        const username = req.body.username;
        const password = req.body.password;
        const customerId = req.body.customerId;
        const role = req.body.role;
        let user = await userService.update(userId, username, email, password, role, customerId);
        res.json(user);
    } catch (err) {
        next(err);
    }
}

exports.deleteUser = async (req, res, next) => {
    try {
        const id = req.params.id;
        await userService.delete(id);
        res.status(204).json();
    } catch (error) {
        next(error);
    }
}