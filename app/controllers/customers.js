const customerService = require('../services/customers');

exports.createCustomer = async (req, res, next) => {
    try {
        const name = req.body.name;
        const address = req.body.address;
        const phoneNumber = req.body.phoneNumber;
        const customer = await customerService.create(name, address, phoneNumber);
        res.status(201).json(customer);
    } catch (err) {
        next(err);
    }
}

exports.getAllCustomers = async (req, res, next) => {
    try {
        const customers = await customerService.getAll();
        res.status(200).json(customers);
    } catch (err) {
        next(err);
    }
}

exports.getCustomerById = async (req, res, next) => {
    try {
        const customerId = req.params.id;
        const customer = await customerService.getById(customerId);
        res.status(200).json(customer);
    } catch (err) {
        next(err);
    }
}

exports.updateCustomer = async (req, res, next) => {
    try {
        const customerId = req.params.id;
        const name = req.body.name;
        const address = req.body.address;
        const phoneNumber = req.body.phoneNumber;
        const editedCustomer = await customerService.update(customerId, name, address, phoneNumber);
        res.status(200).json(editedCustomer);
    } catch (err) {
        next(err);
    }
}

exports.deleteCustomer = async (req, res, next) => {
    try {
        const customerId = req.params.id;
        await customerService.delete(customerId);
        res.status(204).json();
    } catch (err) {
        next(err);
    }
}

exports.getUsersForCustomer = async (req, res, next) => {
    try {
        const customerId = req.params.id;
        const result = await customerService.getUsers(customerId);
        res.status(200).json(result);
    } catch (err) {
        next(err);
    }
}

exports.getServicesForCustomer = async (req, res, next) => {
    try {
        const customerId = req.params.id;
        const result = await customerService.getServices(customerId);
        res.status(200).json(result);
    } catch (err) {
        next(err);
    }
}