const servicesService = require('../services/services');

exports.getAllServices = async (req, res, next) => {
    try {
        const result = await servicesService.getAll();
        res.status(200).json(result);
    } catch (error) {
        next(error);
    }
}

exports.createService = async (req, res, next) => {
    try {
        const name = req.body.name;
        const description = req.body.description;
        const customerId = req.body.customerId;
        const result = await servicesService.create(name, description, customerId);
        res.status(201).json(result);
    } catch (error) {
        next(error);
    }
}


exports.getServiceById = async (req, res, next) => {
    try {
        const serviceId = req.params.id;
        const result = await servicesService.getById(serviceId);
        res.json(200).json(result);
    } catch (err) {
        next(err);
    }
}

exports.updateService = async (req, res, next) => {
    try {
        throw new Error("Not implemented");
    } catch (err) {
        next(err);
    }
}

exports.deleteService = async (req, res, next) => {
    try {
        const serviceId = req.params.id;
        await servicesService.delete(serviceId);
        res.status(204).json();
    } catch (err) {
        next(err);
    }
}
