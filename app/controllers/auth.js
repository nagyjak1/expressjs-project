const authService = require("../services/auth");
const e = require("express");

exports.login = async (req, res, next) => {
    try {
        const username = req.body.username;
        const password = req.body.password;
        let result = await authService.login(username, password);
        res.json({token: result});
    } catch (err) {
        next(err);
    }
}

exports.verify = async (req, res, next) => {
    try {
        const token = req.headers.authorization;
        const user = await authService.verify(token);
        res.status(200).json(user);
    } catch (err) {
        next(err);
    }
}