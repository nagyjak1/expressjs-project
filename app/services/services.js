const Service = require('../models/Service');
const Customer = require('../models/Customer');

exports.create = async (name, description, customerId) => {
    const customer = await Customer.findByPk(customerId);
    if (!customer) {
        const error = new Error('Customer with given id was not found');
        error.httpStatusCode = 400;
        throw error;
    }
    return await Service.create({name, description, customerId});
}

exports.getAll = async () => {
    return await Service.findAll();
}

exports.getById = async (id) => {
    const service = await Service.findByPk(id);
    if (!service) {
        const error = new Error('Service with given id was not found');
        error.httpStatusCode = 404;
        throw error;
    }
    return service;
}

exports.delete = async (id) => {
    const service = await Service.findByPk(id);
    if (!service) {
        const error = new Error('Service with given id was not found');
        error.httpStatusCode = 404;
        throw error;
    }
    await service.destroy();
}