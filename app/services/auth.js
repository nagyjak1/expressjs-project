const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');

const User = require('../models/user');

exports.verify = async (token) => {
    if (!token) {
        const error = new Error('Token is missing');
        error.httpStatusCode = 401;
        throw error;
    }
    token = token.split(' ')[1];

    let result;
    try {
        result = await jwt.verify(token, process.env.SECRET);
    } catch (err) {
        const error = new Error('Token is invalid or expired');
        error.httpStatusCode = 401;
        throw error;
    }
    const user = User.findByPk(result.user.id);
    if (!user) {
        const error = new Error('User not found');
        error.httpStatusCode = 404;
        throw error;
    }
    return user;
}

exports.login = async (username, password) => {
    let foundUser = await User.scope('withPassword').findOne({where: {username: username}});
    if (!foundUser) {
        const error = new Error('A user with this username was not found.');
        error.statusCode = 400;
        throw error;
    }
    let passMatch = await bcryptjs.compare(password, foundUser.password);
    if (!passMatch) {
        const error = new Error('Wrong password!');
        error.statusCode = 401;
        throw error;
    }
    return jwt.sign({
        user: foundUser
    }, process.env.SECRET, {expiresIn: '1h'});
}