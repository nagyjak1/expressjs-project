const User = require('../models/user');
const bcryptjs = require("bcryptjs");
const Customer = require("../models/customer");

exports.getAll = async () => {
    return await User.findAll();
}

exports.create = async (username, email, password, role, customerId) => {
    const customer = await Customer.findByPk(customerId);
    if (!customer) {
        const error = new Error('Customer with given id was not found');
        error.httpStatusCode = 400;
        throw error;
    }
    const hashedPassword = await bcryptjs.hash(password, 10);
    return await User.create({
        username: username, email: email, password: hashedPassword, role: role, customerId: customerId
    });
}

exports.getById = async (id) => {
    const user = await User.findByPk(id);
    if (!user) {
        const error = new Error('User does not exist!');
        error.httpStatusCode = 404;
        throw error;
    }
    return user;
}

exports.update = async (userId, username, email, password, role, customerId) => {
    throw new Error('Not implemented');
}

exports.delete = async (id) => {
    const user = await User.findByPk(id);
    if (!user) {
        const error = new Error('User does not exist!');
        error.httpStatusCode = 404;
        throw error;
    }
    await user.destroy();
}