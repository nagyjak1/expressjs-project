const Customer = require('../models/customer');
const e = require("express");

exports.create = async (name, address, phoneNumber) => {
    return await Customer.create({name, address, phoneNumber});
}

exports.getAll = async () => {
    return await Customer.findAll();
}

exports.getById = async (id) => {
    const customer = await Customer.findByPk(id);
    if (!customer) {
        const error = new Error('Customer with given id was not found');
        error.httpStatusCode = 404;
        throw error;
    }
    return customer;
}

exports.update = async (id, name, address, phoneNumber) => {
    const customer = await Customer.findByPk(id);
    if (!customer) {
        const error = new Error('Customer with given id was not found');
        error.httpStatusCode = 404;
        throw error;
    }
    return await customer.update({name, address, phoneNumber});
}

exports.delete = async (id) => {
    const customer = await Customer.findByPk(id);
    if (!customer) {
        const error = new Error('Customer with given id was not found');
        error.httpStatusCode = 404;
        throw error;
    }
    await customer.destroy();
}

exports.getServices = async (id) => {
    const customer = await Customer.findByPk(id);
    if (!customer) {
        const error = new Error('Customer with given id was not found');
        error.httpStatusCode = 404;
        throw error;
    }
    return await customer.getServices();
}

exports.getUsers = async (id) => {
    const customer = await Customer.findByPk(id);
    if (!customer) {
        const error = new Error('Customer with given id was not found');
        error.httpStatusCode = 404;
        throw error;
    }
    return await customer.getUsers();
}