require('dotenv').config();

const express = require('express');
const app = express();

const {authenticate} = require('./middlewares/auth');
const errorMiddleware = require('./middlewares/error');
const loggerMiddleware = require('./middlewares/logger');
const { openApiSpecification, swaggerUi } = require('./utils/swagger');
const bodyParser = require('body-parser');


const userRoutes = require('./routes/users');
const authRoutes = require('./routes/auth');
const serviceRoutes = require('./routes/services');
const customerRoutes = require('./routes/customers');

// parse body as json in all requests
app.use(bodyParser.json());

// use logger middleware in all requests
app.use(loggerMiddleware);

// set important headers for the REST api to work
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,PATCH,POST,DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
});

// app routes
app.use('/auth', authRoutes);
app.use('/users', authenticate, userRoutes);
app.use('/customers', authenticate, customerRoutes);
app.use('/services', authenticate, serviceRoutes);

// SWAGGER api documentation endpoint
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(openApiSpecification));

// use error handling middleware in all requests
app.use(errorMiddleware);

module.exports = app;