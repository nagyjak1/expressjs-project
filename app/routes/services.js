const express = require('express');
const router = express.Router();

const serviceController = require('../controllers/services');
const authMiddleware = require('../middlewares/auth');

/**
 * @swagger
 * tags:
 *   name: Services
 *   description: Service management endpoints
 */


/**
 * @swagger
 * /services:
 *   post:
 *     summary: Create a new service
 *     tags: [Services]
 *     security:
 *       - BearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/CreateService'
 *     responses:
 *       201:
 *         description: Service created successfully
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Service'
 *       400:
 *         $ref: '#/responses/BadRequestError'
 *       401:
 *         $ref: '#/responses/UnauthorizedError'
 *       403:
 *         $ref: '#/responses/ForbiddenError'
 *       500:
 *         $ref: '#/responses/InternalServerError'
 */
router.post('/', authMiddleware.isAdmin, serviceController.createService);




/**
 * @swagger
 * /services:
 *   get:
 *     summary: Get all services
 *     tags: [Services]
 *     security:
 *       - BearerAuth: []
 *     responses:
 *       200:
 *         description: List of services
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Service'
 *       401:
 *         $ref: '#/responses/UnauthorizedError'
 *       403:
 *         $ref: '#/responses/ForbiddenError'
 *       500:
 *         $ref: '#/responses/InternalServerError'
 */
router.get('/', authMiddleware.isAdmin, serviceController.getAllServices);



/**
 * @swagger
 * /services/{id}:
 *   get:
 *     summary: Get service by ID
 *     tags: [Services]
 *     security:
 *       - BearerAuth: []
 *     parameters:
 *       - name: id
 *         in: path
 *         required: true
 *         schema:
 *           type: integer
 *           example: 1
 *     responses:
 *       200:
 *         description: Service found
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Service'
 *       404:
 *         description: Service not found
 *       401:
 *         $ref: '#/responses/UnauthorizedError'
 *       403:
 *         $ref: '#/responses/ForbiddenError'
 *       500:
 *         $ref: '#/responses/InternalServerError'
 */
router.get('/:id', authMiddleware.isAdmin, serviceController.getServiceById);



/**
 * @swagger
 * /services/{id}:
 *   put:
 *     summary: Update service by ID
 *     tags: [Services]
 *     security:
 *       - BearerAuth: []
 *     parameters:
 *       - name: id
 *         in: path
 *         required: true
 *         schema:
 *           type: integer
 *           example: 1
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/EditService'
 *     responses:
 *       200:
 *         description: Service updated successfully
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Service'
 *       400:
 *         $ref: '#/responses/BadRequestError'
 *       404:
 *         description: Service not found
 *       401:
 *         $ref: '#/responses/UnauthorizedError'
 *       403:
 *         $ref: '#/responses/ForbiddenError'
 *       500:
 *         $ref: '#/responses/InternalServerError'
 */
router.put('/:id', authMiddleware.isAdmin, serviceController.updateService);



/**
 * @swagger
 * /services/{id}:
 *   delete:
 *     summary: Delete service by ID
 *     tags: [Services]
 *     security:
 *       - BearerAuth: []
 *     parameters:
 *       - name: id
 *         in: path
 *         required: true
 *         schema:
 *           type: integer
 *           example: 1
 *     responses:
 *       204:
 *         description: Service deleted successfully
 *       404:
 *         description: Service not found
 *       401:
 *         $ref: '#/responses/UnauthorizedError'
 *       403:
 *         $ref: '#/responses/ForbiddenError'
 *       500:
 *         $ref: '#/responses/InternalServerError'
 */
router.delete('/:id', authMiddleware.isAdmin, serviceController.deleteService);

module.exports = router;

