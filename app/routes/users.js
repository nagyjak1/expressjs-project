let express = require('express');
let router = express.Router();

const auth = require('../middlewares/auth');

const userController = require('../controllers/users');

/**
 * @swagger
 * tags:
 *   name: Users
 *   description: User management endpoints
 */


/**
 * @swagger
 * /users:
 *   post:
 *     summary: Create a new user
 *     tags: [Users]
 *     security:
 *       - BearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/CreateUser'
 *     responses:
 *       201:
 *         description: User created successfully
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/User'
 *       400:
 *         $ref: '#/responses/BadRequestError'
 *       401:
 *         $ref: '#/responses/UnauthorizedError'
 *       403:
 *         $ref: '#/responses/ForbiddenError'
 *       500:
 *         $ref: '#/responses/InternalServerError'
 */
router.post('/', auth.isAdmin, userController.createUser);


/**
 * @swagger
 * /users:
 *   get:
 *     summary: Get all users
 *     tags: [Users]
 *     security:
 *       - BearerAuth: []
 *     responses:
 *       200:
 *         description: List of users
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/User'
 *       401:
 *         $ref: '#/responses/UnauthorizedError'
 *       403:
 *         $ref: '#/responses/ForbiddenError'
 *       500:
 *         $ref: '#/responses/InternalServerError'
 */
router.get('/', auth.isAdmin, userController.getAllUsers);



/**
 * @swagger
 * /users/{id}:
 *   get:
 *     summary: Get user by ID
 *     tags: [Users]
 *     security:
 *       - BearerAuth: []
 *     parameters:
 *       - name: id
 *         in: path
 *         required: true
 *         schema:
 *           type: integer
 *           example: 1
 *     responses:
 *       200:
 *         description: User found
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/User'
 *       404:
 *         description: User not found
 *       401:
 *         $ref: '#/responses/UnauthorizedError'
 *       403:
 *         $ref: '#/responses/ForbiddenError'
 *       500:
 *         $ref: '#/responses/InternalServerError'
 */
router.get('/:id', auth.isRequestingOwnData, userController.getUserById);



/**
 * @swagger
 * /users/{id}:
 *   put:
 *     summary: Update user by ID
 *     tags: [Users]
 *     security:
 *       - BearerAuth: []
 *     parameters:
 *       - name: id
 *         in: path
 *         required: true
 *         schema:
 *           type: integer
 *           example: 1
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/EditUser'
 *     responses:
 *       200:
 *         description: User updated successfully
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/User'
 *       400:
 *         $ref: '#/responses/BadRequestError'
 *       404:
 *         description: User not found
 *       401:
 *         $ref: '#/responses/UnauthorizedError'
 *       403:
 *         $ref: '#/responses/ForbiddenError'
 *       500:
 *         $ref: '#/responses/InternalServerError'

 */
router.put('/:id', auth.isAdmin, userController.updateUser);



/**
 * @swagger
 * /users/{id}:
 *   delete:
 *     summary: Delete user by ID
 *     tags: [Users]
 *     security:
 *       - BearerAuth: []
 *     parameters:
 *       - name: id
 *         in: path
 *         required: true
 *         schema:
 *           type: integer
 *           example: 1
 *     responses:
 *       204:
 *         description: User deleted successfully
 *       404:
 *         description: User not found
 *       401:
 *         $ref: '#/responses/UnauthorizedError'
 *       403:
 *         $ref: '#/responses/ForbiddenError'
 *       500:
 *         $ref: '#/responses/InternalServerError'
 */
router.delete('/:id', auth.isAdmin, userController.deleteUser);



module.exports = router;
