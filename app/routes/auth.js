const express = require('express');
const router = express.Router();

const authController = require('../controllers/auth');

/**
 * @swagger
 * tags:
 *   name: Customers
 *   description: Customer management endpoints
 */


/**
 * @swagger
 * /auth/login:
 *   post:
 *     tags: [Auth]
 *     summary: Log in a user and return a JWT token
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               username:
 *                 type: string
 *                 example: johndoe
 *               password:
 *                 type: string
 *                 format: password
 *                 example: password123
 *     responses:
 *       '200':
 *         description: Successful login with JWT token
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 token:
 *                   type: string
 *                   example: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9
 *       '400':
 *         description: A user with this username was not found
 *       '401':
 *         description: Wrong password
 */
router.post('/login', authController.login);

/**
 * @swagger
 * /auth/verify:
 *   get:
 *     tags: [Auth]
 *     summary: Verify the JWT token and return the user info
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       '200':
 *         description: JWT is valid, and the user is returned
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/User'
 *       '401':
 *         description: Token is missing, invalid, or expired
 *       '404':
 *         description: User not found
 */
router.get('/verify', authController.verify);

module.exports = router;