const express = require('express');
const router = express.Router();

const customerController = require('../controllers/customers');
const authMiddleware = require('../middlewares/auth');

/**
 * @swagger
 * tags:
 *   name: Customers
 *   description: Customer management endpoints
 */



/**
 * @swagger
 * /customers:
 *   post:
 *     summary: Create a new customer
 *     tags: [Customers]
 *     security:
 *       - BearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/CreateCustomer'
 *
 *     responses:
 *       201:
 *         description: Customer created successfully
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Customer'
 *       400:
 *         $ref: '#/responses/BadRequestError'
 *       401:
 *         $ref: '#/responses/UnauthorizedError'
 *       403:
 *         $ref: '#/responses/ForbiddenError'
 *       500:
 *         $ref: '#/responses/InternalServerError'
 */
router.post('/', authMiddleware.isAdmin, customerController.createCustomer);




/**
 * @swagger
 * /customers:
 *   get:
 *     summary: Get all customers
 *     tags: [Customers]
 *     security:
 *       - BearerAuth: []
 *     responses:
 *       200:
 *         description: List of customers
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Customer'
 *       401:
 *         $ref: '#/responses/UnauthorizedError'
 *       403:
 *         $ref: '#/responses/ForbiddenError'
 *       500:
 *         $ref: '#/responses/InternalServerError'
 */
router.get('/', authMiddleware.isAdmin, customerController.getAllCustomers);



/**
 * @swagger
 * /customers/{id}:
 *   get:
 *     summary: Get customer by ID
 *     tags: [Customers]
 *     security:
 *       - BearerAuth: []
 *     parameters:
 *       - name: id
 *         in: path
 *         required: true
 *         schema:
 *           type: integer
 *           example: 1
 *     responses:
 *       200:
 *         description: Customer found
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Customer'
 *       404:
 *         description: Customer not found
 *       401:
 *         $ref: '#/responses/UnauthorizedError'
 *       403:
 *         $ref: '#/responses/ForbiddenError'
 *       500:
 *         $ref: '#/responses/InternalServerError'
 */
router.get('/:id', authMiddleware.isRequestingAssignedCustomersData, customerController.getCustomerById);



/**
 * @swagger
 * /customers/{id}/users:
 *   get:
 *     summary: Get users for a specific customer
 *     tags: [Customers]
 *     security:
 *       - BearerAuth: []
 *     parameters:
 *       - name: id
 *         in: path
 *         required: true
 *         schema:
 *           type: integer
 *           example: 1
 *     responses:
 *       200:
 *         description: List of users for the customer
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/User'
 *       404:
 *         description: Customer not found
 *       401:
 *         $ref: '#/responses/UnauthorizedError'
 *       403:
 *         $ref: '#/responses/ForbiddenError'
 *       500:
 *         $ref: '#/responses/InternalServerError'
 */
router.get('/:id/users', authMiddleware.isRequestingAssignedCustomersData, customerController.getUsersForCustomer);



/**
 * @swagger
 * /customers/{id}/services:
 *   get:
 *     summary: Get services for a specific customer
 *     tags: [Customers]
 *     security:
 *       - BearerAuth: []
 *     parameters:
 *       - name: id
 *         in: path
 *         required: true
 *         schema:
 *           type: integer
 *           example: 1
 *     responses:
 *       200:
 *         description: List of services for the customer
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Service'
 *       404:
 *         description: Customer not found
 *       401:
 *         $ref: '#/responses/UnauthorizedError'
 *       403:
 *         $ref: '#/responses/ForbiddenError'
 *       500:
 *         $ref: '#/responses/InternalServerError'
 */
router.get('/:id/services', authMiddleware.isRequestingAssignedCustomersData, customerController.getServicesForCustomer);



/**
 * @swagger
 * /customers/{id}:
 *   put:
 *     summary: Update customer by ID
 *     tags: [Customers]
 *     security:
 *       - BearerAuth: []
 *     parameters:
 *       - name: id
 *         in: path
 *         required: true
 *         schema:
 *           type: integer
 *           example: 1
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/EditCustomer'
 *     responses:
 *       200:
 *         description: Customer updated successfully
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Customer'
 *       400:
 *         $ref: '#/responses/BadRequestError'
 *       404:
 *         description: Customer not found
 *       401:
 *         $ref: '#/responses/UnauthorizedError'
 *       403:
 *         $ref: '#/responses/ForbiddenError'
 *       500:
 *         $ref: '#/responses/InternalServerError'
 */
router.put('/:id', authMiddleware.isAdmin, customerController.updateCustomer);



/**
 * @swagger
 * /customers/{id}:
 *   delete:
 *     summary: Delete customer by ID
 *     tags: [Customers]
 *     security:
 *       - BearerAuth: []
 *     parameters:
 *       - name: id
 *         in: path
 *         required: true
 *         schema:
 *           type: integer
 *           example: 1
 *     responses:
 *       204:
 *         description: Customer deleted successfully
 *       404:
 *         description: Customer not found
 *       401:
 *         $ref: '#/responses/UnauthorizedError'
 *       403:
 *         $ref: '#/responses/ForbiddenError'
 *       500:
 *         $ref: '#/responses/InternalServerError'
 */
router.delete('/:id', authMiddleware.isAdmin, customerController.deleteCustomer);

module.exports = router;
