const bcryptjs = require("bcryptjs");
const User = require("../models/user");

const logger = require("./logger");

exports.addAdmin = async (username, password, email) => {
    try {
        if (await User.findOne({where: {username: username}})) {
            logger.info("Default admin already exists");
            return
        }
        const hashedPassword = await bcryptjs.hash(password, 10);
        await User.create({
            username: username, email: email, password: hashedPassword, role: 'admin'
        });
        logger.info('Added default admin user')
    } catch (e) {
        logger.error('Problem with seeding');
        logger.error(e);
    }
}