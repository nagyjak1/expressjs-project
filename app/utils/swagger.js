const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

const options = {
    failOnErrors: true,
    definition: {

        openapi: '3.0.0',

        info: {
            title: 'Customer and Services Management API',
            version: '1.0.0',
            description: 'API for managing customers, services, and users with admin and customer roles.'
        },

        components: {
            securitySchemes: {
                bearerAuth: {
                    type: 'http',
                    scheme: 'bearer',
                    bearerFormat: 'JWT',
                },
            },
        },

        responses: {
            'UnauthorizedError': {
                description: 'The request requires user authentication.',
            },
            'InternalServerError': {
                description: 'The server encountered an unexpected condition that prevented it from fulfilling the request.'
            },
            'ForbiddenError': {
                description: 'The server understood the request but refuses to authorize it.'
            },
            'BadRequestError': {
                description: 'The request could not be understood or was missing required parameters.'
            }
        },

        security: [
            {
                bearerAuth: []
            }
        ],
    },
    apis: [`${__dirname}/../routes/*.js`, `${__dirname}/../models/*.js`],
}

const openApiSpecification = swaggerJSDoc(options);

module.exports = { openApiSpecification, swaggerUi };