const app = require('./app')
const db = require('./app/models/index')
const logger = require('./app/utils/logger')

const dbSeeder = require('./app/utils/db_seeder');


// wait for database scheme synchronization
db.sync()
    .then(() => {

        // add default admin user
        return dbSeeder.addAdmin(
            process.env.ADMIN_USERNAME || 'admin',
            process.env.ADMIN_PASSWORD || 'admin',
            process.env.ADMIN_EMAIL || 'admin@email.com',)
    })
    .then(() => {
        // start server
        const port = process.env.PORT || 3000
        app.listen(port);
        logger.info(`Server started on port ${port}`);
    }).catch(err => {
        logger.error(err);
    })
